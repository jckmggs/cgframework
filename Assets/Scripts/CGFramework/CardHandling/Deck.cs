using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGFramework.CardHandling
{
    /// <summary>
    /// A list of cards populated from COPIES of the CardData ScriptableObjects in the CardDatabase - allowing for multiple or modified cards. 
    /// </summary>
    public class Deck : MonoBehaviour
    {
        private List<CardCore.CardData> cards = new List<CardCore.CardData>();

        public void AddCard(CardCore.CardData card)
        {
            cards.Add(card);
        }

        public void RemoveCard(int index)
        {
            cards.RemoveAt(index);
        }
    }
}