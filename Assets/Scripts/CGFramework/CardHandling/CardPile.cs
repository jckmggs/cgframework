using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGFramework.CardHandling
{
    /// <summary>
    /// A list of cards that REFERENCES CardData ScriptableObjects in a deck
    /// </summary>
    public class CardPile
    {
        private List<CardCore.CardData> cards = new List<CardCore.CardData>();

        public void AddCard(CardCore.CardData card)
        {
            cards.Add(card);
        }

        public void InsertCard(CardCore.CardData card, int index)
        {
            cards.Insert(index, card);
        }

        public void MoveCard(CardPile moveTo, int fromIndex)
        {
            CardCore.CardData card = cards[fromIndex];

            moveTo.AddCard(card);
            cards.Remove(card);
        }

        public void MoveCard(CardPile moveTo, int fromIndex, int toIndex)
        {
            CardCore.CardData card = cards[fromIndex];

            moveTo.InsertCard(card, toIndex);
            cards.Remove(card);
        }
    }
}