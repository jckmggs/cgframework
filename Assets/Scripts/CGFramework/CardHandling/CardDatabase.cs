﻿using System.Collections.Generic;
using UnityEngine;

namespace CGFramework.CardHandling
{
    public class CardDatabase : Core.DataManager<int, CardCore.CardData>
    {       
        [SerializeField] private string resourcesItemsFolder = default;

        private void Awake()
        {
            LoadFromResources();
        }

        private void LoadFromResources()
        {
            dataDictionary = new Dictionary<int, CardCore.CardData>();
            CardCore.CardData[] itemsFromResources = Resources.LoadAll<CardCore.CardData>(resourcesItemsFolder);
            foreach (var itemData in itemsFromResources)
            {
                TryPutDataItem(itemData.Id, itemData);
            }
        }
    }
}