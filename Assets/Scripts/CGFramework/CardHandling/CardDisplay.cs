using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CGFramework.CardHandling
{
    public class CardDisplay : MonoBehaviour
    {
        [SerializeField] private CardCore.CardData card;

        [SerializeField] private Sprite cardSprite;
        [SerializeField] private TextMeshPro cardName;
        [SerializeField] private TextMeshPro cardDescription;

        void Start()
        {
            cardName.text = card.DisplayName;
            cardDescription.text = card.Description;
        }
    }
}