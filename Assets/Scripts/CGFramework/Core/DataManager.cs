﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CGFramework.Core
{
    public abstract class DataManager<T, D> : MonoBehaviour
    {
        protected Dictionary<T, D> dataDictionary = default;

        public Dictionary<T, D> GetAllDataObjects()
        {
            return dataDictionary.ToDictionary(k => k.Key, v => v.Value);
        }
        public D GetDataObject(T id)
        {
            if(dataDictionary.ContainsKey(id))
            {
                return dataDictionary[id];
            }

            return default;
        }
        protected virtual bool TryPutDataItem(T id, D data)
        {
            if (dataDictionary.ContainsKey(id))
            {
                return false;
            }

            dataDictionary.Add(id, data);
            return true;
        }
        protected virtual bool TryRemoveItem(T id, D data)
        {
            if (!dataDictionary.ContainsKey(id))
            {
                return false;
            }

            dataDictionary.Remove(id);
            return true;
        }
    }
}
