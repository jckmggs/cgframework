﻿using UnityEngine;

namespace CGFramework.CardCore
{
    [CreateAssetMenu(menuName = "Card Game/Cards/Card Data")]
    public class CardData : ScriptableObject
    {
        [SerializeField] private int id = default;
        [SerializeField] public string displayName = default;
        [SerializeField] public string description = default;

        public int Id
        {
            get { return id; }
        }
        public string DisplayName
        {
            get { return displayName; }
        }

        public string Description
        {
            get { return description; }
        }
    }
}
